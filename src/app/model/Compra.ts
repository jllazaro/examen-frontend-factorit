import { Carrito } from './Carrito'
import { Usuario } from './Usuario'

export class Compra {
    id: number
    carrito: Carrito
    cliente: Usuario

}