import { Carrito } from './Carrito'
import { Producto } from './Producto'

export class Articulo {
    id: number
    producto: Producto
    cantidad: number
    importe : number
    selected: boolean

    constructor(producto: Producto, cantidad: number) {
        this.producto = producto
        this.cantidad = cantidad

    }
}