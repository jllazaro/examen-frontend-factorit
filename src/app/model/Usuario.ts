import { Carrito } from './Carrito'
import { Compra } from './Compra'

export class Usuario {
    id: number
    usuario: string
    password: string
    compras: Compra[]
    carritoActual: Carrito
    vip: boolean
    fechaCambioVip: Date
}