import { Articulo } from './Articulo'

export class Carrito{
    id: number
    tipo:string
    articulos : Articulo[]
    importeSinDescuento: number
    importeTotal: number
}