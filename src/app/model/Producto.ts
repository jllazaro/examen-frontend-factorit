export class Producto {
    id: number
    descripcion: string
    precio: number
    imagen: string
    cantidad: number
}