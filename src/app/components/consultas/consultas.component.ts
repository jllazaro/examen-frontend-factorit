import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from 'src/app/model/Usuario';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-consultas',
  templateUrl: './consultas.component.html',
  styleUrls: ['./consultas.component.css']
})
export class ConsultasComponent implements OnInit {

  usuariosVip = [] as Usuario[]
  usuariosExVip = [] as Usuario[]
  usuariosExNormales = [] as Usuario[]
  mes
  constructor(
    private router: Router,
    private authService: AuthService,
  ) { }


  ngOnInit(): void {
    if (!this.usuario) {
      this.router.navigate(['/login']);
    }
    this.getUsuariosVip()
  }
  getUsuariosVip() {
    console.log("this.authService.getUsuariosVip()")
    this.authService.getUsuariosVip().subscribe(usuarios =>{
      this.usuariosVip = usuarios
    })
  }

  getExUsuariosVipByDate() {
    this.authService.getExUsuariosVipByDate(this.date).subscribe(usuarios =>{
      this.usuariosExVip= usuarios
    })
  }

  getExUsuariosNormalesByDate() {
    this.authService.getExUsuariosNormalesByDate(this.date).subscribe(usuarios =>{
      this.usuariosExNormales= usuarios
    })
  }
  get usuario(): Usuario {
    return this.authService.getUser()
  }
  mesSeleccionado() {
    this.getExUsuariosNormalesByDate()
    this.getExUsuariosVipByDate()
  }
  get date(): Date {
    return this.mes._d
  }
}
function convert(str) {
  var date = new Date(str),
    mnth = ("0" + (date.getMonth() + 1)).slice(-2),
    day = ("0" + date.getDate()).slice(-2);
  return [date.getFullYear(), mnth, day].join("-");
}
