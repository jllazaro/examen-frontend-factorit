import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, RouterLink } from '@angular/router';
import { Subscription } from 'rxjs';
import { Usuario } from 'src/app/model/Usuario';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  registerForm: FormGroup;
  constructor(
    public router: Router,
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) { 
    this.authService.setUser(null)
  }

  ngOnInit(): void {
    this.createForm()
  }

  createForm() {
    this.registerForm = this.formBuilder.group({
      usuario: [null, Validators.required],
      password: [null, Validators.required],
    });
  }

  onSubmit() {
    if (this.registerForm.invalid) {
      return;
    }
    this.authService.login(this.registerForm.value as Usuario).subscribe((response) => {
      if (response) {
        console.log(response)
        this.authService.setUser(response)
        this.router.navigate(['/productos']);
      }
    })
  }

}
