import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Articulo } from 'src/app/model/Articulo';
import { Producto } from 'src/app/model/Producto';
import { Usuario } from 'src/app/model/Usuario';
import { AuthService } from 'src/app/services/auth.service';
import { CarritoService } from 'src/app/services/carrito.service';
import { ProductoService } from 'src/app/services/producto.service';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {

  productos: Producto[]
  cantidad: number
  constructor(
    private router: Router,
    private authService: AuthService,
    private productoService: ProductoService,
    private carritoService: CarritoService
  ) { }


  ngOnInit(): void {
    if (!this.usuario) {
      this.router.navigate(['/login']);
    }

    this.productoService.getAll().subscribe((response) => {
      this.productos = response
    })
  }

  get usuario(): Usuario {
    return this.authService.getUser()
  }

  agregarAlCarrito(producto: Producto) {
    console.log(producto)
    this.carritoService.agregarCarritoUsuario(this.usuario.id, new Articulo(producto, producto.cantidad)).subscribe(carrito => {

    })
  }
}
