import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Articulo } from 'src/app/model/Articulo';
import { Carrito } from 'src/app/model/Carrito';
import { Usuario } from 'src/app/model/Usuario';
import { AuthService } from 'src/app/services/auth.service';
import { CarritoService } from 'src/app/services/carrito.service';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.css']
})
export class CarritoComponent implements OnInit {
  carrito = {} as Carrito;

  constructor(
    private router: Router,
    private authService: AuthService,
    private carritoService: CarritoService
  ) { }


  ngOnInit(): void {
    if (!this.usuario) {
      this.router.navigate(['/login']);
    }
    this.cargarCarrito()
  }

  cargarCarrito() {
    this.carritoService.getCarrito(this.usuario.id).subscribe(carrito => {
      console.log(carrito)
      this.carrito = carrito
    });
  }

  get usuario(): Usuario {
    return this.authService.getUser()
  }

  actualizarArticulo(articulo: Articulo) {
    this.carritoService.actualizarArticuloCarrito(this.carrito.id, articulo).subscribe(carrito => {
      console.log(carrito)
      this.carrito = carrito
    })
  }

  eliminarArticulo(articulo: Articulo) {
    this.carritoService.eliminarArticuloCarrito(this.carrito.id, articulo).subscribe(carrito => {
      console.log(carrito)
      this.carrito = carrito
    })
  }

  finalizarCompra(){
    console.log(this.usuario)
    this.carritoService.finalizarCompra(this.carrito.id, this.usuario).subscribe(carrito => {
      console.log(carrito)
      this.carrito = {} as Carrito
    })
  }

  eliminarCarrito(){
    this.carritoService.eliminarCarrito(this.carrito.id).subscribe(carrito => {
      console.log(carrito)
      this.carrito = {} as Carrito
    })

  }

}
