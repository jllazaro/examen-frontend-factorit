import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Usuario } from 'src/app/model/Usuario';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  user: Usuario = null
  constructor(private authService: AuthService) { }

  ngOnInit(): void { }

  get usuario(): Usuario {
    return this.authService.getUser()
  }
  logout(){
    this.authService.setUser(null);
  }
}
