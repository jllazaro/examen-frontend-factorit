import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CarritoComponent } from '../components/carrito/carrito.component';
import { ConsultasComponent } from '../components/consultas/consultas.component';
import { LoginComponent } from '../components/login/login.component';
import { ProductosComponent } from '../components/productos/productos.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'productos', component: ProductosComponent },
  { path: 'carrito', component: CarritoComponent},
  { path: 'consultas', component: ConsultasComponent},
  { path: '**', component: LoginComponent}
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class RouteModule { }
