import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Usuario } from '../model/Usuario';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {
  }

  login(usuario: Usuario): Observable<Usuario> {
    return this.http.post<Usuario>('api/usuario/login', usuario)
  }

  setUser(usuario: Usuario) {
    localStorage.setItem("usuario", JSON.stringify(usuario))
  }

  getUser(): Usuario {
    return JSON.parse(localStorage.getItem("usuario")) as Usuario
  }
  getExUsuariosNormalesByDate(date: Date): Observable<Usuario[]> {
    return this.http.post<Usuario[]>('api/usuario/getExUsuariosNormalesByDate', date)
  }
  getExUsuariosVipByDate(date: Date): Observable<Usuario[]> {
    return this.http.post<Usuario[]>('api/usuario/getExUsuariosVipByDate', date)
  }
  getUsuariosVip(): Observable<Usuario[]> {
    return this.http.get<Usuario[]>('api/usuario/getUsuariosVip')
  }
}
