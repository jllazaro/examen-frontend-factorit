
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Producto } from '../model/Producto';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {
  constructor(private http: HttpClient) {
  }

  getAll(): Observable<Producto[]> {
    return this.http.get<Producto[]>('api/producto')
  }
}
