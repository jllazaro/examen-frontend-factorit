import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Articulo } from '../model/Articulo';
import { Carrito } from '../model/Carrito';
import { Usuario } from '../model/Usuario';

@Injectable({
  providedIn: 'root'
})
export class CarritoService {
  constructor(private http: HttpClient) {
  }

  agregarCarritoUsuario(id: number, articulo: Articulo): Observable<Carrito> {
    return this.http.post<Carrito>('api/carrito/' + id, articulo)
  }
  getCarrito(id: number): Observable<Carrito> {
    return this.http.get<Carrito>('api/carrito/' + id)
  }

  actualizarArticuloCarrito(id: number, articulo: Articulo): Observable<Carrito> {
    return this.http.post<Carrito>('api/carrito/actualizarArticulo/' + id, articulo)
  }

  eliminarArticuloCarrito(id: number, articulo: Articulo): Observable<Carrito> {
    return this.http.post<Carrito>('api/carrito/eliminarArticuloCarrito/' + id, articulo)
  }

  eliminarCarrito(id: number) : Observable<Carrito> {
    return this.http.delete<Carrito>('api/carrito/eliminarCarrito/' + id)
  }

  finalizarCompra(id: number, usuario: Usuario) : Observable<Carrito> {
    return this.http.post<Carrito>('api/carrito/finalizarCompra/' + id, usuario)
  }

}

